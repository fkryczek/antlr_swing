tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje; separator=\" \n\"> 
start: <name;separator=\" \n\"> ";

decl  :
        ^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodawanie(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmowanie(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> mnozenie(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> dzielenie(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> podstawienie(p1={$ID.text},p2={$e2.st})
        | ID                       -> id(p1={$ID.text})
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | ^(FOR e1=expr e2=expr) {numer++;}     -> for(p1={$e1.st}, p2={$e2.st}, n={numer.toString()})
        | ^(DO     e1=expr e2=expr) {numer++;}  -> do(p1={$e1.st}, p2={$e2.st}, n={numer.toString()})
        | ^(WHILE  e1=expr e2=expr) {numer++;}  -> while(p1={$e1.st}, p2={$e2.st}, n={numer.toString()})
        | ^(EQ   e1=expr e2=expr)  -> rownosc(p1={$e1.st}, p2={$e2.st})
        | ^(NEQ   e1=expr e2=expr) -> nierownosc(p1={$e1.st}, p2={$e2.st})
    ;
    